function implement(target, ...interfaces) {
    for (let interface of interfaces){
        for (let key of Object.keys(interface)){
            if (typeof interface[key] === "object"){
                if (interface[key] instanceof Array){
                    for (let possibleChild of interface[key]){
                        if (!(target[key] instanceof Array)){
                            throw Error("Interface check failed '"+key+": "+target[key]+"' is not of type array<"+interface[key]+">")
                        }
                        for (let subItem of target[key]){
                            implement(subItem, possibleChild);
                        }
                    }
                }
                else{
                    implement(target[key], interface[key]);
                }
            }
            else if (typeof interface[key]==="string"){
                if (implement.types[interface[key]]!==undefined){
                    if (!implement.types[interface[key]](target[key])){
                        throw Error("Interface check failed '"+key+": "+target[key]+"' is not of type "+interface[key]);
                    }
                }
                else{
                    let oneMatched = false;
                    for (let type of regexTypes){
                        if (type.test(interface[key])){
                            oneMatched = true;
                            if (!implement.types[type.toString()](target[key])){
                                throw Error("Interface check failed '"+key+": "+target[key]+"' is not of type "+interface[key]);
                            }
                        }
                    }
                    if (!oneMatched){
                        throw Error("type "+interface[key]+" is not defined")
                    }
                }
            }
            else{
                throw Error("Invalid interface. Interface includes non-string items.");
            }
        }
    }
};
implement.types = {};
implement.regexTypes = [];
implement.addType = function (type, testFunction){
    this.types[String(type)] = testFunction;
    if (type instanceof RegExp){
        this.regexTypes.push(type);
    }
}
implement.addType("string",(item)=>{
    return (typeof item === "string");
})
implement.addType("object",(item)=>{
    return (typeof item === "object");
})
implement.addType("number",(item)=>{
    return (typeof item === "number");
})
implement.addType(/^array<.*>$/, (type, item)=>{
    if (item instanceof Array){
        type = type.slice(6,type.length-1)
        for (let subItem of item){
            if (implement.types[type]!==undefined){
                if (!implement.types[type](subItem, type)){
                    return false;
                }
            }
            else{
                let oneMatched = false;
                for (let type of regexTypes){
                    if (type.test(interface[key])){
                        oneMatched = true;
                        if (!implement.types[type.toString()](subItem)){
                            return false;
                        }
                    }
                }
                if (!oneMatched){
                    throw Error("type "+type+" is not defined");
                }
            }
        }
        return true;
    }
    else{
        return false;
    }
})
module.exports = implement;
